const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.status(200).send('Hello, Express.js');
});

app.get('/hello', function (req, res) {
    res.status(200).send('Hello, stranger');
});

app.get('/hello/:name', function (req, res) {
    res.status(200).send(`Hello, ${req.params.name}!`);
});

app.all('/sub/:param1?/:param2', function (req, res) {
    res.status(200).send([req.protocol, '://', req.get('host'), req.originalUrl].join(''));
});

app.post('/post', checkKey, sendPost);

function sendPost(req, res) {
    console.log(Object.keys(req.body).length);
    if(Object.keys(req.body).length > 0) {
        console.log(req.body);
        res.status(200).send(req.body);
    } else {
        console.log(req.body);
        res.sendStatus(404);
    }
}

function checkKey(req, res, next) {
    if(req.headers['key']){
        next();
    } else {
        res.sendStatus(404);
    }
}

app.listen(3000, function () {
    console.log('Server is listening port 3000')
});