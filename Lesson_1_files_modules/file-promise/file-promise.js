const fs = require('fs');

const conf = {encoding: 'UTF-8'};

exports.read = function (file) {
  return new Promise((done, fail) => {
    fs.readFile(file, conf, (err, data)=>{
      if(err) {
        fail(err);
      } else {
        done(data);
      }
    });
  });
};

exports.write = function (file, data) {
  return new Promise((done, fail) => {
    fs.writeFile(file, data, (err)=>{
      if(err) {
        fail(err);
      } else {
        done(data);
      }
    });
  });
};

