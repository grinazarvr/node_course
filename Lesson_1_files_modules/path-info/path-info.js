const fs = require('fs');

const conf = {encoding: 'UTF-8'};

exports.pathInfo = function (path, callback) {
  fs.stat(path, (err, state) => {
    if (err) {
      callback(err, null);
    } else {
      if (state.isFile()) {
        fs.readFile(path, conf, (err, info) => {
          if (err) {
            callback(err, null);
          } else {
            callback(null, {
              type: 'file',
              path: path,
              content: info
            });
          }
        });
      } else if (state.isDirectory()) {
        fs.readdir(path, (err, files) => {
          if (err) {
            callback(err, null);
          } else {
            callback(null, {
              type: 'directory',
              path: path,
              childs: files
            });
          }
        });
      } else {
        callback(null, {path});
      }
    }
  });
};