const fs = require('fs');
const path = require('path');
const { read } = require('../file-promise/file-promise');


exports.readAll = function (filePath) {
  return getFiles(filePath).then((files) => Promise.all(getReadFilesPromises(filePath, files)));
};

const getReadFilesPromises = (filePath, files) => {
  return files.map((file) =>
      read(path.join(filePath, file))
        .then((content) => ({name: file, content: content}))
        .catch((err) => err)
  );
};

const getFiles = function (path) {
  return new Promise((done, fail) => {
    fs.readdir(path, (err, files) => {
      if(err) {
        fail(err);
      } else {
        done(files);
      }
    });
  });
};