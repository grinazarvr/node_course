const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

app.post("/rpc", function(req, res) {
  if (req.body.method) {
    const method = RPC[req.body.method];
    method(req.body.params, function (error, result) {
      if (result) {
        res.status(200).json(result);
      } else {
        res.sendStatus(error.status)
      }
    })
  } else {
    res.sendStatus(400);
  }
});

let RPC = {
  getUsers: function (params, calback) {
    if (users) {
      calback(null, users);
    } else {
      calback({status: 500}, null);
    }
  },

  getUser: function (params, calback) {
    if (users) {
      let user = users.filter(user => user.id === parseInt(params.id))[0];
      if (user) {
        calback(null, user);
      } else {
        calback({status: 400}, null);
      }
    } else {
      calback({status: 500}, null);
    }
  },

  addUser: function (params, calback) {
    if (users) {
      if (params.name && params.score) {
        let maxId = 0;
        users.forEach(user => {maxId = maxId > user.id ? maxId : user.id});
        let user = {id: maxId + 1, name: params.name, score: params.score};
        users.push(user);
        calback(null, user);
      } else {
        calback({status: 400}, null);
      }
    } else {
      calback({status: 500}, null);
    }
  },

  changeUser: function (params, calback) {
    if (users) {
      let user = users.filter(user => user.id === parseInt(params.id))[0];
      if (user) {
        if (params.name) {
          user.name = params.name;
        }
        if (params.score) {
          user.score = params.score;
        }
        calback(null, user);
      } else {
        calback({status: 400}, null);
      }
    } else {
      calback({status: 500}, null);
    }
  },

  deleteUser: function (params, calback) {
    if (users) {
      let user = users.filter(user => user.id === parseInt(params.id))[0];
      if (user) {
        users.splice(users.indexOf(user), 1);
        calback(null, user);
      } else {
        calback({status: 400}, null);
      }
    } else {
      calback({status: 500}, null);
    }
  },

  deleteUsers: function (params, calback) {
    if (users) {
        users = [];
        calback(null, users);
    } else {
      calback({status: 500}, null);
    }
  },
};

app.listen(3000, function () {
  console.log('Server is listening port 3000')
});