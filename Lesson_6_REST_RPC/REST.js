const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

const apiV1 = express.Router();

apiV1.get('/users/', function (req, res) {
  if (users) {
    console.log(req.query);
    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);
    if(offset && limit) {
      res.status(200).json(users.slice(offset, offset + limit))
    } else {
      res.status(200).json(users);
    }
  } else {
    res.sendStatus(500);
  }
});

apiV1.get('/users/:id', function (req, res) {
  if (users) {
    let user = users.filter(user => user.id === parseInt(req.params.id))[0];
    if (user) {
      res.status(200).json(user);
    } else {
      res.sendStatus(400);
    }
  } else {
    res.sendStatus(500);
  }
});

apiV1.post('/users/', function (req, res) {
  if (users) {
    if (req.body.name && req.body.score) {
      let maxId = 0;
      users.forEach(user => {maxId = maxId > user.id ? maxId : user.id});
      let user = {id: maxId + 1, name: req.body.name, score: req.body.score};
      users.push(user);
      res.status(200).json(user);
    } else {
      res.sendStatus(400);
    }
  } else {
    res.sendStatus(500);
  }
});

apiV1.put('/users/:id', function (req, res) {
  if (users) {
    let user = users.filter(user => user.id === parseInt(req.params.id))[0];
    if (user) {
      if (req.body.name) {
        user.name = req.body.name;
      }
      if (req.body.score) {
        user.score = req.body.score;
      }
      res.status(200).json(user);
    } else {
      res.sendStatus(400);
    }
  } else {
    res.sendStatus(500);
  }
});

apiV1.delete('/users/:id', function (req, res) {
  if (users) {
    let user = users.filter(user => user.id === parseInt(req.params.id))[0];
    if (user) {
      users.splice(users.indexOf(user), 1);
      res.sendStatus(200);
    } else {
      res.sendStatus(400);
    }
  } else {
    res.sendStatus(500);
  }
});


app.use('/api', apiV1);
app.listen(3000, function () {
  console.log('Server is listening port 3000')
});