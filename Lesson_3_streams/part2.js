const crypto = require('crypto');
const fs = require('fs');
const Transform = require('stream').Transform;



class CTransform extends Transform {
  constructor(options) {
    super(options)
  }

  _transform(chunk, encoding, callback) {
    this.push(chunk.toString('hex'));
    callback();
  }
}

const hash = crypto.createHash('md5');//.setEncoding('hex');

const input = fs.createReadStream('./text.txt');
const output = fs.createWriteStream('./result.txt');
const transformToHex = new  CTransform();


const encodedAndHexedInputStream = input.pipe(hash).pipe(transformToHex);

encodedAndHexedInputStream.pipe(process.stdout);
encodedAndHexedInputStream.pipe(output);

