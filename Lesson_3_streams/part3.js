const Transform = require('stream').Transform;
const Readable = require('stream').Readable;
const Writable = require('stream').Writable;


class CReadable extends Readable {
  constructor(options) {
    super(options)
  }

  _read(size) {
    this.push(Math.random().toString());
  }
}

class CWritable extends Writable {
  constructor(options) {
    super(options)
  }

  _write(chunk, encoding, callback) {
      console.log(chunk.toString());
      callback();
  }
}

class CTransform extends Transform {
  constructor(options) {
    super(options)
  }

  _transform(chunk, encoding, callback) {
    this.push(chunk.toString() + ' transform worked!');
    setTimeout(callback, 1000);
  }
}

const read = new  CReadable();
const write = new  CWritable();
const transform = new  CTransform();

read
  .pipe(transform)
  .pipe(write);
