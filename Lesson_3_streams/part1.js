const crypto = require('crypto');
const fs = require('fs');

const hash = crypto.createHash('md5');

const input = fs.createReadStream('./text.txt');
const output = fs.createWriteStream('./result.txt');

const encodedInputStream = input.pipe(hash);

encodedInputStream.pipe(process.stdout);
encodedInputStream.pipe(output);

