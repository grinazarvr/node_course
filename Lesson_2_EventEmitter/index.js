const ChatApp = require('./chat-app');

let webinarChat =  new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat =       new ChatApp('---------vk');

let chatOnMessage = (message) => {
  console.log(message);
};

let prepareChatMessage = (defaultMessage) => {
  let chatName = defaultMessage.split(':')[0];
  console.log(`${chatName}: Готовлюсь к ответу`);
};

let closeMessage = (chatName) => {
  console.log(`Чат ${chatName} закрылся :(`);
};

vkChat.setMaxListeners(2);

webinarChat.on('message', chatOnMessage);
facebookChat.on('message', chatOnMessage);
vkChat.on('message', chatOnMessage);
webinarChat.prependListener('message', prepareChatMessage);
vkChat.prependListener('message', prepareChatMessage);
vkChat.on('close', closeMessage);

// Закрыть вконтакте
setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
  vkChat.close();
  vkChat.removeListener('message', chatOnMessage);
  vkChat.removeListener('message', prepareChatMessage);
}, 10000 );


// Закрыть фейсбук
setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
  facebookChat.removeListener('message', chatOnMessage);
}, 15000 );

// Закрыть вебинар
setTimeout( ()=> {
  console.log('Закрываю вебинар!');
  webinarChat.removeListener('message', chatOnMessage);
  webinarChat.removeListener('message', prepareChatMessage);
}, 30000 );