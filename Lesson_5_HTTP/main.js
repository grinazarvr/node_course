const http = require('http');
const querystring = require('querystring');
const port = 3000;

function gettingHash(firstName, lastName) {

  let options = {
    host: 'netology.tomilomark.ru',
    headers: {'firstname': firstName},
    path: '/api/v1/hash',
    method: 'POST'
  };

  let data = querystring.stringify({'lastName': lastName});
  return new Promise((resolve, reject) => {
    const request = http.request(options);
    request
      .on('response', (response) => {
        let data = '';
        response.on('data', (chunk) => {
          data += chunk;
        });
        response.on('end', () => {
          resolve(data);
        })
      })
      .on('error', (err) => {
        console.log(err);
        reject(err);
      });
    request.write(data);
    request.end();
  })
}

let server = http.createServer();
server
  .on('error', (err) => console.log(err))
  .on('request', (req, res) => {
    let headers = req.headers;
    if (headers.lastname && headers.firstname) {
      gettingHash(headers.firstname, headers.lastname)
        .then((data) => {
          res.writeHead(200, 'OK', {'Content-Type': 'text/plain'});
          let response = {
            "firstName": headers.firstname,
            "lastName": headers.lastname,
            "secretKey": JSON.parse(data)['hash']
          };
          res.write(JSON.stringify(response));
          res.end();
        });

    }
  })
  .on('listening', () => console.log(`Server started at ${port} port`));

server.listen(port);